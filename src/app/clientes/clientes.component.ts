import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[] = [
    {id: 1, name: 'Jesús Enrique', lastName: 'Cañedo Urías', createdAt: '2019-09-03', email: 'enriquecanedo1@gmail.com'},
    {id: 2, name: 'Ejemplo 2', lastName: 'Apellido 2', createdAt: '2019-09-04', email: 'ejemplo21@gmail.com'},
    {id: 3, name: 'Ejemplo 3', lastName: 'Apellido 2', createdAt: '2019-09-05', email: 'ejemplo3@gmail.com'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
